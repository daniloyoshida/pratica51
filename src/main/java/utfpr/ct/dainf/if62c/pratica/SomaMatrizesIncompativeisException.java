package utfpr.ct.dainf.if62c.pratica;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Danilo
 */
public class SomaMatrizesIncompativeisException extends MatrizesIncompativeisException{
    int m1Linha, m2Linha, m1Coluna, m2Coluna;
    public SomaMatrizesIncompativeisException(Matriz m1, Matriz m2) {
        super(m1, m2);
        m1Linha = m1.getMat().length;
        m2Linha = m2.getMat().length;
        m1Coluna = m1.getMat()[0].length;
        m2Coluna = m2.getMat()[0].length;
    }
    
    @Override
    public String getLocalizedMessage() {
        String bla = String.format("Matrizes de %dx%d e %dx%d não podem ser somadas", m1Linha, m1Coluna, m2Linha, m2Coluna);
        return bla;
    }
}
